#coding:utf-8
#Name: allocate.py
#Author: Jouevemau
#Version: 0.9
#Last modified: 2017-08-24
#Purpose: 给每个人分配周一到周六的工作, 每天都有一个白班(标记为1),需要分配靠前的工作;
#			一个夜班(标记为2)，需要分配靠后的工作；一个下夜班(标记为3)，也要分配靠前
#			的工作。剩下的分配给其他人。每天总共有34件工作。
#Data:agenda.csv，第一行为标题，从前往后分别为“个数，周一，周二，周三，周四，周五，周六，周日”;
#			第一列是每个人需要分配的工作件数。

import numpy as np
import random
import csv

file = open('agenda.csv', 'rb')
agenda = np.loadtxt(file, delimiter = ',', skiprows = 1)
file.close()

m, n = agenda.shape #获得agenda矩阵的维度。注意这里没有方括号，这是与octave的区别
allocation = np.zeros((m + 1, n), dtype = int) 
#建立一个分配结果的空矩阵，在最后面为炎哥添加一行
allocation = allocation.astype(np.dtype('S30')) #把allocation转化为元素为字符串的矩阵
#注意，这里如果用.astype(np.str)的话默认长度是11个字节，会把结果截取掉一部分

for i in range(1, n): #从列"周一"开始循环
	workz = range(1, 15) #中班从1~14中选择
	workf = range(20, 29) #副班从20~28中选择
	#剩下的凑够33个
	worko = range(1, 35)
	worko.pop(32)
	worko = list(set(worko).difference(set(workz)))
	worko = list(set(worko).difference(set(workf)))	
	#剩下的凑够33个
	#---------下面是中班的代码块--------------
	rowz = np.where(agenda[:, i] == 1)[0][0] #获得每天中班所在的行数
	countz = int(agenda[rowz, 0]) #获得当天中班写报告的个数
	z = random.sample(workz, countz) #中班先选择
	z.sort()
	allocation[rowz, i] = '.'.join([str(d) for d in z]) #为了保存为csv的时候不出问题，这里的分隔设置为.而不是,
	workz = list(set(workz).difference(set(z))) #把中班获取的从workz中剔除
	#---------下面是下夜班的代码块-------------
	rowy = np.where(agenda[:, i] == 3)[0][0] 
	county = int(agenda[rowy, 0]) 
	y = random.sample(workz, county)
	y.sort()
	allocation[rowy, i] = '.'.join([str(d) for d in y])
	workz = list(set(workz).difference(set(y)))
	#---------下面是副班的代码块---------------
	rowf = np.where(agenda[:, i] == 2)[0][0] 
	countf = int(agenda[rowf, 0]) 
	f = random.sample(workf, countf)
	f.sort()
	allocation[rowf, i] = '.'.join([str(d) for d in f])
	workf = list(set(workf).difference(set(f)))
	#------------------------------------------
	worko += workz + workf #剩下的就给其他人分啦
	for j in range(m):
		counto = int(agenda[j, 0])
		if counto >= 1 and allocation[j, i] == '0':
			o = random.sample(worko, counto)
			o.sort()
			allocation[j, i] = '.'.join([str(d) for d in o])
			worko = list(set(worko).difference(set(o)))
	allocation[m, i] = '33' #这一行是炎哥的		
	
	# #check一下有没有错误
	checks = []
	for j in range(m + 1):
		checks += allocation[j, i].split('.')
		checkl = [int(d) for d in checks]	
	checkl.sort()
	if checkl != range(1, 35):
		print "Error! Exiting!"
		break
	
allocation = allocation[:, range(1, n)] #去除多余的第一列
print allocation

#numpy只能save元素为数字的csv，所以这里用csv模块来save !!!!错!!!!是可以的
file = open('allocation.csv', 'wb')
write = csv.writer(file)
for record in allocation:
	write.writerow(record)
file.close()